﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class Test : MonoBehaviour {

    public GameObject target;
    NavMeshAgent nav;

    private void Awake()
    {
        nav = GetComponent<NavMeshAgent>();
    }

    // Use this for initialization
    void Start () {
	}

    private void Update()
    {
        nav.SetDestination(target.transform.position);
    }
}
