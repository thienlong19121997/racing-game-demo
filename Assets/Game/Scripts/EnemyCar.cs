﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class EnemyCar : MonoBehaviour {

    public GameObject target;

    public int isType2;
    private int num;
    private Transform[] paths = new Transform[4];
    public int[] ar = new int[4];
    public NavMeshAgent nav;

    private void Start()
    {
        target = Car.instance.gameObject;

        if (isType2 == 0)
            return;

        for(int i = 0; i < 4; i++)
        {
            int r = Random.Range(0, EnemyPooler.instance.paths.Length);
            for(int j = 0; j < i; j++)
            {
                if(r == ar[j])
                {
                    r = Random.Range(0, EnemyPooler.instance.paths.Length);
                    j = -1;
                }
            }
            ar[i] = r;
        }

        for(int i = 0; i < 4; i++)
        {
            int n = ar[i];
            paths[i] = EnemyPooler.instance.paths[n];
        }
    }
    float s;
    float targetDistance;
    public float angleRotate;
    float sp = 20;
    public 	void Update () {
        if (Car.instance.isGameOver || Car.instance.isStartGame == false)
        {
            return;
        }
        
        if (isType2 == 0)
        {

            angleRotate = transform.eulerAngles.y;
          
            if (transform.eulerAngles.y <= (45-20) || transform.eulerAngles.y > (315+20))
            {
                nav.speed = sp;
                nav.acceleration = 30;
            }
            else if (transform.eulerAngles.y <= (315-20) && transform.eulerAngles.y > (225+20))
            {
                nav.speed = sp;
                nav.acceleration = 30;
            }
            else if (transform.eulerAngles.y <= (225-20) && transform.eulerAngles.y > (135+20))
            {
                nav.speed = sp;
                nav.acceleration = 30;
            }
            else if (transform.eulerAngles.y <= (135-20) && transform.eulerAngles.y > (45+20))
            {
                nav.speed = sp;
                nav.acceleration = 30;
            }
            else
            {
                nav.speed = 4   ;
                nav.acceleration = 15;
            }

            if(Vector3.Distance(transform.position, Car.instance.transform.position) > 35)
            {
                nav.speed = sp;
                nav.acceleration = 30;
            }
         

            if (target != null)
                nav.SetDestination(target.transform.position);
        }
        else
        {

            targetDistance = 25;
            float distanceToPlayer = Vector3.Distance(transform.position, Car.instance.transform.position);
            if(distanceToPlayer < targetDistance)
            {
                targetDistance = 35;

                if (s != 5)
                {
                    nav.speed = 15;
                    nav.acceleration = 10;
                    s = 5;
                }

                nav.SetDestination(target.transform.position);
            }
            else
            {
                if (s != 4)
                {
                    nav.speed = 5;
                    nav.acceleration = 15;
                    s = 4;
                }

                nav.SetDestination(paths[num].transform.position);
                float distanceBetweenPath = Vector3.Distance(transform.position, paths[num].transform.position);
                if(distanceBetweenPath < 1)
                {
                    num++;
                    if (num == paths.Length)
                        num = 0;
                }
            }
        }


    }





}
