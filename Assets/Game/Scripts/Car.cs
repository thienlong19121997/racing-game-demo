﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.AI;
using UnityEngine.UI;


public class Car : MonoBehaviour {

    public static Car instance;

    [Header("Info")]
    public int myLevel;
    public float distanceTarget;
    public float myDistance;
    public bool isStartGame;
    public bool isGameOver;
    public bool isBreak;
    public bool isDrift;
    public bool isDirection;

    [Header("Physic")]
    private Rigidbody rig;
    public float moveSpeed;
    public float speedAfterDrift;
    private float smooth;
    private float dir;
    private Vector3 directionDrift;
    private float timeFakeVelocity;
    private float valueDrift;
    public float maxValueDrift;

    [Header("GameObject")]
    public TrailRenderer[] trail_drift;
    public TrailRenderer[] trail_drift_2;
    public ParticleSystem parSmoke;
    public GameObject directionDriftObject;
    public GameObject cameraMain;
    public GameObject taptoplayObject;
    public GameObject gameOverMenu;
    public GameObject completeMenu;
    private Vector3 offsetCamera;
    // distance value manager

    public RectTransform highDistance;
    public RectTransform myCarDistance;
    public Image barDistance;
    public Text myHighDistanceText;
    public Text myDistanceText;
    public Text targetDistanceText;
    public Text myLevelText;
    public Text targetLevelText;
    Vector3 a_position_distance, b_position_distance;
    float c_magnitute_ab;

    // gameover 
    public Text distanceText_GO;
    public Text bestText_GO;
    public Text coinEarn_GO;

    public bool isResetData;

    private void Awake()
    {
        if (isResetData)
        {
            PlayerPrefs.DeleteAll();
        }
        instance = this;
        rig = GetComponent<Rigidbody>();
    }

    private void Start () {
        offsetCamera = transform.position - cameraMain.transform.position;
        InitGame();
        
      //  StartGame();

    }

    private void FixedUpdate ()
    {
        if (isStartGame == false || isGameOver)
            return;

        CarController();
    }

    private void Update()
    {
        if (isStartGame == false || isGameOver)
            return;


        DistanceManage();
        RiftControl();
    }


    private void LateUpdate()
    {
        CameraMainController();
    }

    private void InitGame()
    {
        SetTrailDrift(false,true);
        SetTrailDrift(false, false);

        myLevel = PlayerPrefs.GetInt("mylevel");

        if(myLevel >= LevelManager.instance.levels.Count)
        {
            myLevel = Random.Range(0, LevelManager.instance.levels.Count);
        }
        distanceTarget = LevelManager.instance.levels[myLevel].distance;

        transform.position = LevelManager.instance.levels[myLevel].startPosition.transform.position;

        Vector3 temp = transform.eulerAngles;
        temp.y = LevelManager.instance.levels[myLevel].angle;
        transform.eulerAngles = temp;

        Vector3 targetPositon = new Vector3(transform.position.x, cameraMain.transform.position.y, transform.position.z);
        cameraMain.transform.position = targetPositon;

        for(int i = 0; i < LevelManager.instance.levels[myLevel].startPositionEnemy.Count; i++)
        {
            GameObject enemy = EnemyPooler.instance.GetObject();

            Vector3 tempe = enemy.transform.eulerAngles;
            tempe.y = transform.eulerAngles.y;
            enemy.transform.position = tempe;

            enemy.GetComponent<EnemyCar>().isType2 = i;
            enemy.SetActive(true);
            enemy.transform.position = LevelManager.instance.levels[myLevel].startPositionEnemy[i].transform.position;

            NavMeshAgent nav = enemy.GetComponent<NavMeshAgent>();
            nav.enabled = true;

        }

        isStartGame = false;
        isGameOver = false;

        float highdistancedata = PlayerPrefs.GetFloat("highdistance" + PlayerPrefs.GetInt("mylevel"));

        if (highdistancedata == 0)
        {
            highDistance.gameObject.SetActive(false);
        }
        else
        {
            Vector2 tempHighDistance = highDistance.anchoredPosition;
            tempHighDistance.x = Mathf.Lerp(-160.0f, 160.0f, (float)highdistancedata / distanceTarget);
            highDistance.anchoredPosition = tempHighDistance;

            myHighDistanceText.text = "" + (int)highdistancedata;
        }

        Vector2 tempmyCarDistance = myCarDistance.anchoredPosition;
        tempmyCarDistance.x = -160.0f;
        myCarDistance.anchoredPosition = tempmyCarDistance;

        myDistance = 0;
        a_position_distance = b_position_distance = transform.position;
        c_magnitute_ab = 0;
        myDistanceText.text = "0";
        targetDistanceText.text = distanceTarget + "";

        barDistance.fillAmount = 0;

        myLevelText.text = "Level " + (PlayerPrefs.GetInt("mylevel")+1);
        targetLevelText.text = "" + (PlayerPrefs.GetInt("mylevel") + 2);
    }
    
    public void SetSmoke(bool isSet)
    {
        if (isSet)
        {
            parSmoke.Play();
        }
        else
        {
            parSmoke.Pause();
        }
    }

    public void SetTrailDrift(bool status,bool isLeft)
    {
        if (isLeft)
        {
            for (int i = 0; i < 2; i++)
            {
                trail_drift[i].emitting = status;
            }
        }
        else
        {
            for (int i = 0; i < 2; i++)
            {
                trail_drift_2[i].emitting = status;
            }
        }
   
    }

    public void StartGame()
    {
        taptoplayObject.SetActive(false);
        isStartGame = true;

    }


    private void DistanceManage()
    {
        a_position_distance = transform.position;
        c_magnitute_ab = (a_position_distance - b_position_distance).magnitude;
        b_position_distance = transform.position;

        myDistance += c_magnitute_ab*.5f;
        myDistanceText.text = (int)myDistance + "" ;
        barDistance.fillAmount = myDistance / (float)(distanceTarget);

        Vector2 temp = myCarDistance.anchoredPosition;
        temp.x = Mathf.Lerp(-160, 160, barDistance.fillAmount);
        myCarDistance.anchoredPosition = temp;

        float highdistancedata = PlayerPrefs.GetFloat("highdistance" + PlayerPrefs.GetInt("mylevel"));

        if (myDistance > highdistancedata)
        {
            PlayerPrefs.SetFloat("highdistance" + PlayerPrefs.GetInt("mylevel"),myDistance);

            Vector2 tempHighDistance = highDistance.anchoredPosition;
            tempHighDistance.x = Mathf.Lerp(-160.0f, 160.0f, (float)myDistance / distanceTarget);
            highDistance.anchoredPosition = tempHighDistance;

            if (highDistance.gameObject.activeSelf)
            {
                highDistance.gameObject.SetActive(false);
            }
        }


        if ((int)myDistance >= distanceTarget)
        {
            Debug.Log("complete level");
            Complete();
        }
    }

    private void Complete()
    {
        isGameOver = true;
        int lvl = PlayerPrefs.GetInt("mylevel");
        lvl++;
        PlayerPrefs.SetInt("mylevel", lvl);

        completeMenu.SetActive(true);
    }

    public float timeFake;
    float tTimeScale;
    private void CarController()
    {
        if (isBreak == false)
        {
            if (smooth < 1)
                smooth += (Time.deltaTime * 1f * speedAfterDrift * .7f);
        }

        if (isDirection == false)
        {
            if(timeFake > 0)
            {
               // Time.timeScale = Mathf.Lerp(1, .4f , tTimeScale/timeFake);
                timeFake -= Time.deltaTime;
            }
            else
            {
             //   if (Time.timeScale != 1)
             //   Time.timeScale = 1;
                Vector3 directionMove = directionDriftObject.transform.position - transform.position;
                rig.velocity = directionMove.normalized * moveSpeed * smooth;
            }



        }
        else
        {
            if (timeFakeVelocity > 0)
            {
                timeFakeVelocity -= (Time.deltaTime * .7f);
            }
            // transform.position += moveSpeed * Time.deltaTime * directionDrift.normalized * timeFakeVelocity;

            // Vector3 directionMove = directionDriftObject.transform.position - transform.position;
            //  rig.velocity = directionMove.normalized * moveSpeed * smooth;

        }

        if (isDrift)
        {
            if (smoothRift < 1)
                smoothRift += Time.deltaTime;
        }
        else
        {
            if (smoothRift > 0)
            {
                if (smoothRift > .2f)
                    smoothRift = .2f;

                smoothRift -= Time.deltaTime;
            }
            else
            {
                smoothRift = 0;
            }
        }




        if (isDrift)
        {
           if(isDoubleHolding)
            {
                maxValueDrift = 36000;
            }
            else
            {
                maxValueDrift = 36000;
            }
            
           if(rig.velocity.magnitude < 5)
            {
               // maxValueDrift = 720;
            }

            if (valueDrift < maxValueDrift)
               {
                valueDrift += 4 * smoothRift * Mathf.Abs(dir);

                transform.eulerAngles += new Vector3(0, 4 * smoothRift * dir, 0);
            }
        }
        else
        {
            if(timeFake <= 0 && rig.velocity.magnitude > 5 && isCollisionWall == false)
           SmartAutoRotate();
        }


    }



    float smoothRift;


    public bool isHoldingLeft;
    public bool isHoldingRight;
    public bool isDoubleHolding;

    public void RiftControl()
    {
        if(isHoldingLeft)
        {
            if (timeFake < .5f)
            {
                timeFake += Time.deltaTime / 2;
            }

            dir = -1;

            if (smooth > 0)
            {
                smooth -= Time.deltaTime;
                isBreak = true;
            }

            isDrift = true;


            if (isDirection == false)
            {
                isDirection = true;
                directionDrift = directionDriftObject.transform.position - transform.position;
                timeFakeVelocity = smooth;
                valueDrift = 0;
            }

            SetTrailDrift(true, true);

            if(timeFake > .15f)
            SetSmoke(true);
        }

        if (isHoldingRight)
        {
            if (timeFake < .5f)
            {
                timeFake += Time.deltaTime / 2;
            }
            dir = 1;

            if (smooth > 0)
            {
                smooth -= Time.deltaTime;
                isBreak = true;
            }

            isDrift = true;


            if (isDirection == false)
            {
                isDirection = true;
                directionDrift = directionDriftObject.transform.position - transform.position;
                timeFakeVelocity = smooth;
                valueDrift = 0;
            }

            SetTrailDrift(true, false);

            if (timeFake > .15f)
                SetSmoke(true);
        }

    }

    public void OnTapDownLeft()
    {
      

        if (isHoldingRight)
        {
            isDoubleHolding = true;
            return;
        }

        isHoldingLeft = true;

    }

    public void OnTapUpLeft()
    {


        if (isHoldingRight)
            return;

        isHoldingLeft = false;
        isHoldingRight = false;


        isBreak = false;
        isDrift = false;
        isDirection = false;

        isDoubleHolding = false;

        SetTrailDrift(false, true);

        SetSmoke(false);

        if (rig.velocity.magnitude < 2)
        {
            timeFake = 0;
        }

        tTimeScale = timeFake;

    }

    public void OnTapDownRight()
    {
   
        if (isHoldingLeft)
        {
            isDoubleHolding = true;
            return;
        }
        isHoldingRight = true;


    }

 

    public void OnTapUpRight()
    {



        if (isHoldingLeft)
            return;

        isHoldingLeft = false;
        isHoldingRight = false;


        isBreak = false;
        isDrift = false;
        isDirection = false;

        isDoubleHolding = false;

        SetTrailDrift(false, false);
        SetSmoke(false);

        if (rig.velocity.magnitude < 2)
        {
            timeFake = 0;
        }

        tTimeScale = timeFake;
    }

    private void CameraMainController()
    {
        Vector3 targetPositon = new Vector3(transform.position.x - offsetCamera.x, cameraMain.transform.position.y, transform.position.z - offsetCamera.z);
      //  cameraMain.transform.position = Vector3.Lerp(cameraMain.transform.position, targetPositon, 3 * Time.deltaTime);

        cameraMain.transform.position = targetPositon;


        //Vector3 targetAngle = new Vector3(cameraMain.transform.eulerAngles.x, transform.eulerAngles.y, cameraMain.transform.eulerAngles.z);
        //cameraMain.transform.eulerAngles = Vector3.Lerp(cameraMain.transform.eulerAngles, targetAngle, 3 * Time.deltaTime);
    }

    bool isCollisionWall;
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Wall")
        {
            isCollisionWall = true;
            timeFake = 0;
            Debug.Log(" collision wall ");
        }

        if(collision.gameObject.tag == "Enemy")
        {
            Debug.Log(" collision enemy ");

            collision.gameObject.SetActive(false);
            gameObject.SetActive(false);
            GameObject explotion = ExplotionPooler.instance.GetObject();
            explotion.transform.position = gameObject.transform.position;
            explotion.SetActive(true);

            GameObject explotion2 = ExplotionPooler.instance.GetObject();
            explotion2.transform.position = collision.gameObject.transform.position;
            explotion2.SetActive(true);

            

            if (isGameOver)
                return;

            GameOverGame();
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if(collision.gameObject.tag == "Wall")
        {
            isCollisionWall = false;
        }
    }   



    void GameOverGame()
    {
        Debug.Log(" gameover ");
        isGameOver = true;


        distanceText_GO.text = (int)myDistance + "";


        float highdistancedata = PlayerPrefs.GetFloat("highdistance" + PlayerPrefs.GetInt("mylevel"));
        bestText_GO.text = (int)highdistancedata + "";

        gameOverMenu.SetActive(true);

    }





    public void LoadScene()
    {
        SceneManager.LoadScene(0);
    }


    private const float speedSmartRotate = 40.0f;
    private void SmartAutoRotate()
    {

        if (transform.eulerAngles.y <= 45 || transform.eulerAngles.y > 315)
        {
            Vector3 temp = transform.eulerAngles;

            if (transform.eulerAngles.y <= 45)
            {
                if (temp.y > 2)
                {
                    temp.y -= Time.deltaTime * speedSmartRotate;
                }
                else
                {
                    temp.y = 0;
                }
            }
            else
            {
                if (temp.y < 358)
                {
                    temp.y += Time.deltaTime * speedSmartRotate;
                }
                else
                {
                    temp.y = 0;
                }
            }

            transform.eulerAngles = temp;

        }
        else if (transform.eulerAngles.y <= 315 && transform.eulerAngles.y > 225)
        {
            Vector3 temp = transform.eulerAngles;

            if (temp.y > 272)
            {
                temp.y -= Time.deltaTime * speedSmartRotate;
            }
            else if (temp.y < 268)
            {
                temp.y += Time.deltaTime * speedSmartRotate;
            }
            else
            {
                temp.y = 270;
            }

            transform.eulerAngles = temp;
        }
        else if (transform.eulerAngles.y <= 225 && transform.eulerAngles.y > 135)
        {
            Vector3 temp = transform.eulerAngles;

            if (temp.y > 182)
            {
                temp.y -= Time.deltaTime * speedSmartRotate;
            }
            else if (temp.y < 178)
            {
                temp.y += Time.deltaTime * speedSmartRotate;
            }
            else
            {
                temp.y = 180;
            }

            transform.eulerAngles = temp;
        }
        else if (transform.eulerAngles.y <= 135 && transform.eulerAngles.y > 45)
        {
            Vector3 temp = transform.eulerAngles;

            if (temp.y > 92)
            {
                temp.y -= Time.deltaTime * speedSmartRotate;
            }
            else if (temp.y < 88)
            {
                temp.y += Time.deltaTime * speedSmartRotate;
            }
            else
            {
                temp.y = 89;
            }

            transform.eulerAngles = temp;
        }
    }
}
